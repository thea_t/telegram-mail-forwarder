FROM python:3-alpine

RUN apk add py3-pip
COPY . /opt/telegram-mail-forwarder/
RUN pip3 install -r /opt/telegram-mail-forwarder/requirements.txt
WORKDIR /opt/telegram-mail-forwarder
CMD ["python3", "bot.py"]
