from telegram.ext import Application, CommandHandler, MessageHandler
from telegram.ext import filters
from telegram.ext import CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram import MessageEntity
import logging
import datetime
import smtplib
from email.message import EmailMessage
import re
import pytz
import json



class EmailForwarder:
    def __init__(self):
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
        with open('settings.json') as json_data_file:
            self.config = json.load(json_data_file)

        app = Application.builder().token(self.config["token"]).build()
        self.tz = pytz.timezone('Europe/Berlin')

        self.chat_ids = []
        self.loading_chats()

        app.add_handler(CallbackQueryHandler(self.button))
        app.add_handler(CommandHandler('start', self.start))
        app.add_handler(CommandHandler('add', self.add_address))
        app.add_handler(CommandHandler('remove', self.remove_address))
        app.add_handler(MessageHandler(filters.Entity(MessageEntity.MENTION), self.forward))
        app.add_handler(MessageHandler(filters.Command, self.unknown))

        logging.log(logging.INFO, "Starting Email Forwarder Bot..")
        app.run_polling()

    def loading_chats(self):
        for chatid, settings in self.config["chats"].items():
            if chatid != "-1":
                self.chat_ids.append(int(chatid))
                logging.info("Loading Chat %s" %chatid)

    async def start(self, update, context):
        if update.effective_chat.id in self.chat_ids:
            return

        logging.log(logging.INFO, "Chatid: %s" %self.chat_ids)
        print("Context", context.args)
        secret = context.args[0] if context.args and len(context.args) > 0 else ""
        #logging.info("secret: %s" %secret)
        if secret:
            if secret == self.config["authenticate"]:
                logging.info("%s - session authenticated" %update.effective_chat.id)
                context.bot.send_message(chat_id=update.effective_chat.id, text="Hallo, ich leite Telegram Nachrichten an Emails weiter. Makiere mich in der Nachricht, die weitergeleitet werden soll!")
                self.chat_ids.append(update.effective_chat.id) if update.effective_chat.id not in self.chat_ids else self.chat_ids
                self.config["chats"][str(update.effective_chat.id)] = self.config["chats"]["-1"]
                self.write_settings()
                return

        logging.info("%s - secret key is wrong" %update.effective_chat.id)
        context.bot.send_message(chat_id=update.effective_chat.id, text="Der eingegebene Schlüssel ist falsch. Tschüß!")
        if context.bot.get_chat(chat_id=update.effective_chat.id).type != "private":
            context.bot.leave_chat(chat_id=update.effective_chat.id)

    async def add_address(self, update, context):
        if update.effective_chat.id not in self.chat_ids:
            return

        if not context.args:
            return

        mail_address = context.args[0]        
        
        if len(mail_address) <= 7 or not re.match("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}$", mail_address):
            logging.warning("No valid mail address: %s" %mail_address)
            return

        if mail_address not in self.config["chats"][str(update.effective_chat.id)]["addresses"]:
            self.config["chats"][str(update.effective_chat.id)]["addresses"].append(mail_address)
            logging.info("Mail address added: %s" %mail_address)
            self.write_settings()

    async def remove_address(self, update, context):
        if update.effective_chat.id not in self.chat_ids:
            return

        if not context.args:
            return

        if context.args[0] not in self.config["chats"][str(update.effective_chat.id)]["addresses"]:
            logging.warning("Mail address not registered: %s" %context.args[0])
            return

        self.config["chats"][str(update.effective_chat.id)]["addresses"].remove(context.args[0])
        logging.info("Mail address removed: %s" %context.args[0])
        self.write_settings()

    async def forward(self, update, context):
        if update.effective_chat.id not in self.chat_ids:
            logging.warning("Chat is not authorized.")
            return

        if not self.config["chats"][str(update.effective_chat.id)]["forwarding_enabled"]:
            logging.warning("Forwarding disabled.")
            return

        if update["message"]:
            message = update["message"]["text"]
        elif update["edited_message"]:
            message = update["edited_message"]["text"]
        else:
            logging.error("Event is no message or edit.")
            return

        if not message:
            logging.warning("No Message found in Event.")
            return

        if self.config["bot_name"] not in message:
            logging.warning("Bot is not mentioned: ignoring message")
            return

        message = message.replace(self.config["bot_name"], "")
        
        smtp_server = smtplib.SMTP(self.config["mail_server"])
        #smtp_server.set_debuglevel(1)
        smtp_server.starttls()
        smtp_server.ehlo()

        msg_intro = "Dies ist eine weitergeleitete Mail aus der Telegram Gruppe: %s\nBei Probleme an jonny@riotcat.org wenden\n================================================================\n\n" %update.effective_chat.title
        
        logging.info("Sending message in group %s" %update.effective_chat.title)
        for to_addr in self.config["chats"][str(update.effective_chat.id)]["addresses"]:
            msg = EmailMessage()
            msg["to"] = to_addr
            msg["date"] = datetime.datetime.now()
            msg["from"] = self.config["from_address"]
            msg["subject"] = "Telegram Gruppe: %s" %update.effective_chat.title
            msg["Reply-To"] = self.config["reply_to_address"]
            msg.set_content(msg_intro + message, 'plain')
            
            #logging.info("Sending to %s" %to_addr)
            try:
                smtp_server.send_message(msg)
            except Exception as e:
                logging.error("[Error] while sending mail to %s: %s" %(to_addr, e))

        smtp_server.quit()



    async def unknown(self, update, context):
        if update.effective_chat.id not in self.chat_ids:
            return

        keyboard = [[InlineKeyboardButton("Weiterleitung aktivieren", callback_data='enable_forwarding'), 
                    InlineKeyboardButton("Weiterleitung deaktivieren", callback_data='disable_forwarding')],
                    [InlineKeyboardButton("Alle Befehle ansehen", callback_data='help')]
                    ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text("Sorry, ich kenne diesen Befehel nicht.\nAber ich kenne:", reply_markup=reply_markup)

    async def button(self, update, context):
        if update.effective_chat.id not in self.chat_ids:
            return

        query = update.callback_query

        # CallbackQueries need to be answered, even if no notification to the user is needed
        # Some clients may have trouble otherwise. See https://core.telegram.org/bots/api#callbackquery
        query.answer()

        if query.data == "enable_forwarding":
            self.set_setting(update.effective_chat.id, "forwarding_enabled", 1)
            query.edit_message_text(text="Mail Weiterleitung aktiviert!")
        if query.data == "disable_forwarding":
            self.set_setting(update.effective_chat.id, "forwarding_enabled", 0)
            query.edit_message_text(text="Mail Weiterleitung deaktiviert!")
        if query.data == "help":
            query.edit_message_text(text="Alle Befehle:\nEmailadresse hinzufügen\n/add your-email-adress@example.com\n\nEmailadresse entfernen\n/remove your-email-adress@example.com\n\Diese Hilfe: /help")
        

    def write_settings(self):
        with open('settings.json', 'w') as outfile:
            json.dump(self.config, outfile, indent=4, sort_keys=True, default=str)

    def get_setting(self, chatid, setting):
        if chatid in self.chat_ids:
            chat_settings = self.config["chats"][str(chatid)]
            if setting in chat_settings:
                return chat_settings[setting]
        return False

    def set_setting(self, chatid, setting, value):
        if chatid in self.chat_ids:
            chat_settings = self.config["chats"][str(chatid)]
            chat_settings[setting] = value
            self.write_settings()
            return True
        return False

def main():
    bot = EmailForwarder()

main()